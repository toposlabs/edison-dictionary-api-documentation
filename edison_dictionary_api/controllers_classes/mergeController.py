from edison_dictionary_api import EdisonBaseAPI


class mergeController(EdisonBaseAPI):
    """Databases"""

    _controller_name = "mergeController"

    def approve(self, editorId, entityId):
        """

        Args:
            editorId: (string): editorId
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['editorId', 'entityId']
        parameters_names_map = {}
        api = '/merge/approve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def approveAll(self, editorId, **kwargs):
        """

        Args:
            editorId: (string): editorId
            stopOnCollision: (boolean): stopOnCollision

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['editorId', '**kwargs']
        parameters_names_map = {}
        api = '/merge/approveAll'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, editorId, **kwargs):
        """

        Args:
            editorId: (string): editorId
            maxNumber: (integer): maxNumber
            start: (integer): start

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['editorId', '**kwargs']
        parameters_names_map = {}
        api = '/merge/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def merge(self, editorId):
        """

        Args:
            editorId: (string): editorId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['editorId']
        parameters_names_map = {}
        api = '/merge/merge'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def reject(self, editorId, entityId, **kwargs):
        """

        Args:
            editorId: (string): editorId
            entityId: (string): entityId
            note: (string): note

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['editorId', 'entityId', '**kwargs']
        parameters_names_map = {}
        api = '/merge/reject'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def rejectAll(self, editorId, **kwargs):
        """

        Args:
            editorId: (string): editorId
            note: (string): note

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['editorId', '**kwargs']
        parameters_names_map = {}
        api = '/merge/rejectAll'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def write(self, editorId, entity, entityId, **kwargs):
        """

        Args:
            editorId: (string): editorId
            entity: (string): entity
            entityId: (string): entityId
            note: (string): note

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['editorId', 'entity', 'entityId', '**kwargs']
        parameters_names_map = {}
        api = '/merge/write'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
