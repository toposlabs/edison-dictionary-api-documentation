from edison_dictionary_api import EdisonBaseAPI


class topicTypesController(EdisonBaseAPI):
    """Topic types."""

    _controller_name = "topicTypesController"

    def add(self, description, topicId):
        """

        Args:
            description: (string): description
            topicId: (string): topicId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['description', 'topicId']
        parameters_names_map = {}
        api = '/topic-types/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, **kwargs):
        """

        Args:
            topicId: (string): topicId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/topic-types/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, typeId):
        """

        Args:
            typeId: (string): typeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['typeId']
        parameters_names_map = {}
        api = '/topic-types/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, typeId):
        """

        Args:
            typeId: (string): typeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['typeId']
        parameters_names_map = {}
        api = '/topic-types/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
