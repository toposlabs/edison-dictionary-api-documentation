from edison_dictionary_api import EdisonBaseAPI


class doctypesBlacklistKeywordsController(EdisonBaseAPI):
    """Keywords from a blacklist of a doc-type"""

    _controller_name = "doctypesBlacklistKeywordsController"

    def add(self, doctypeLanguageId, keyword):
        """

        Args:
            doctypeLanguageId: (string): doctypeLanguageId
            keyword: (string): keyword

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeLanguageId', 'keyword']
        parameters_names_map = {}
        api = '/doc-types-blacklist-keywords/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, doctypeLanguageId, **kwargs):
        """

        Args:
            doctypeLanguageId: (string): doctypeLanguageId
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sortOrderAscending: (boolean): true = ascending (default); false = descending

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/doc-types-blacklist-keywords/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/doc-types-blacklist-keywords/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/doc-types-blacklist-keywords/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
