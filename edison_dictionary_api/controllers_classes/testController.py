from edison_dictionary_api import EdisonBaseAPI


class testController(EdisonBaseAPI):
    """Test Controller"""

    _controller_name = "testController"

    def test(self, argInteger, argIntegerArray, argString, argStringArray, **kwargs):
        """

        Args:
            argInteger: (integer): argInteger
            argIntegerArray: (array): argIntegerArray
            argIntegerArrayOptional: (array): argIntegerArrayOptional
            argIntegerOptional: (integer): argIntegerOptional
            argString: (string): argString
            argStringArray: (array): argStringArray
            argStringArrayOptional: (array): argStringArrayOptional
            argStringOptional: (string): argStringOptional

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['argInteger', 'argIntegerArray', 'argString', 'argStringArray', '**kwargs']
        parameters_names_map = {}
        api = '/test/test'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def testRequest(self, **kwargs):
        """

        Args:
            booleanArg: (boolean): booleanArg
            doubleArg: (number): doubleArg
            intArg: (integer): intArg
            longArg: (integer): longArg
            stringArg: (string): stringArg

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/test/testRequest'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def testTask(self, **kwargs):
        """

        Args:
            booleanArg: (boolean): booleanArg
            doubleArg: (number): doubleArg
            intArg: (integer): intArg
            longArg: (integer): longArg
            sleepMs: (integer): sleepMs
            stringArg: (string): stringArg

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/test/testTask'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
