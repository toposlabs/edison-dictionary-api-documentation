from edison_dictionary_api import EdisonBaseAPI


class themesLanguagesController(EdisonBaseAPI):
    """Themes languages"""

    _controller_name = "themesLanguagesController"

    def add(self, languageId, themeId):
        """

        Args:
            languageId: (string): languageId
            themeId: (string): themeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', 'themeId']
        parameters_names_map = {}
        api = '/themes-languages/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, themeId):
        """

        Args:
            themeId: (string): themeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeId']
        parameters_names_map = {}
        api = '/themes-languages/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, themeLanguageId):
        """

        Args:
            themeLanguageId: (string): themeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeLanguageId']
        parameters_names_map = {}
        api = '/themes-languages/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, themeLanguageId):
        """

        Args:
            themeLanguageId: (string): themeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeLanguageId']
        parameters_names_map = {}
        api = '/themes-languages/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
