from edison_dictionary_api import EdisonBaseAPI


class doctypesKeywordsController(EdisonBaseAPI):
    """Doc-types keywords"""

    _controller_name = "doctypesKeywordsController"

    def add(self, doctypeLanguageId, keyword, weight):
        """

        Args:
            doctypeLanguageId: (string): doctypeLanguageId
            keyword: (string): keyword
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeLanguageId', 'keyword', 'weight']
        parameters_names_map = {}
        api = '/doc-types-keywords/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, keywordId, weight):
        """

        Args:
            keywordId: (string): keywordId
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId', 'weight']
        parameters_names_map = {}
        api = '/doc-types-keywords/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, doctypeLanguageId, **kwargs):
        """

        Args:
            doctypeLanguageId: (string): doctypeLanguageId
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sortMode: (string): { "name", "weight" }
            sortOrderAscending: (boolean): true = ascending (default); false = descending

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/doc-types-keywords/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def listForAllDoctypes(self, **kwargs):
        """

        Args:
            languageId: (string): languageId
            minLevel: (number): minLevel

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/doc-types-keywords/listForAllDoctypes'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/doc-types-keywords/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/doc-types-keywords/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
