from edison_dictionary_api import EdisonBaseAPI


class emotionsBlacklistKeywordsController(EdisonBaseAPI):
    """Keywords from a blacklist of a emotions"""

    _controller_name = "emotionsBlacklistKeywordsController"

    def add(self, emotionLanguageId, keyword):
        """

        Args:
            emotionLanguageId: (string): emotionLanguageId
            keyword: (string): keyword

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionLanguageId', 'keyword']
        parameters_names_map = {}
        api = '/emotions-blacklist-keywords/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, emotionLanguageId, **kwargs):
        """

        Args:
            emotionLanguageId: (string): emotionLanguageId
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sortOrderAscending: (boolean): true = ascending (default); false = descending

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/emotions-blacklist-keywords/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/emotions-blacklist-keywords/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/emotions-blacklist-keywords/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
