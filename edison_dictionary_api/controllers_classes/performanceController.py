from edison_dictionary_api import EdisonBaseAPI


class performanceController(EdisonBaseAPI):
    """Text processing performance measurement"""

    _controller_name = "performanceController"

    def enable(self, enabled):
        """

        Args:
            enabled: (boolean): enabled

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['enabled']
        parameters_names_map = {}
        api = '/performance/enable'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/performance/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
