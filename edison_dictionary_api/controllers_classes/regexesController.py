from edison_dictionary_api import EdisonBaseAPI


class regexesController(EdisonBaseAPI):
    """Regexes."""

    _controller_name = "regexesController"

    def add(self, entityId, regex):
        """

        Args:
            entityId: (string): entityId
            regex: (string): regex

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId', 'regex']
        parameters_names_map = {}
        api = '/regexes/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, entityId):
        """

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/regexes/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, regexId):
        """

        Args:
            regexId: (string): regexId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['regexId']
        parameters_names_map = {}
        api = '/regexes/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, regexId):
        """

        Args:
            regexId: (string): regexId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['regexId']
        parameters_names_map = {}
        api = '/regexes/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
