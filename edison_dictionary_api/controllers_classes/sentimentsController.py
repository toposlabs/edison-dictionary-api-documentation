from edison_dictionary_api import EdisonBaseAPI


class sentimentsController(EdisonBaseAPI):
    """Sentiments"""

    _controller_name = "sentimentsController"

    def add(self, code, **kwargs):
        """

        Args:
            code: (string): code
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['code', '**kwargs']
        parameters_names_map = {}
        api = '/sentiments/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/sentiments/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, sentimentId):
        """

        Args:
            sentimentId: (string): sentimentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentId']
        parameters_names_map = {}
        api = '/sentiments/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, sentimentId):
        """

        Args:
            sentimentId: (string): sentimentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentId']
        parameters_names_map = {}
        api = '/sentiments/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
