from edison_dictionary_api import EdisonBaseAPI


class emotionsController(EdisonBaseAPI):
    """Emotions"""

    _controller_name = "emotionsController"

    def add(self, code, **kwargs):
        """

        Args:
            code: (string): code
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['code', '**kwargs']
        parameters_names_map = {}
        api = '/emotions/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/emotions/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, emotionId):
        """

        Args:
            emotionId: (string): emotionId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionId']
        parameters_names_map = {}
        api = '/emotions/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, emotionId):
        """

        Args:
            emotionId: (string): emotionId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionId']
        parameters_names_map = {}
        api = '/emotions/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
