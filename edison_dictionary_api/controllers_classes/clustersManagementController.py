from edison_dictionary_api import EdisonBaseAPI


class clustersManagementController(EdisonBaseAPI):
    """Clusters management"""

    _controller_name = "clustersManagementController"

    def clusterize(self, clusterLanguageId, **kwargs):
        """

        Args:
            clusterLanguageId: (string): clusterLanguageId
            iterations: (integer): iterations
            maxClusters: (integer): maxClusters
            maxNames: (integer): maxNames
            minClusters: (integer): minClusters
            seed: (integer): seed

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/clusters-management/clusterize'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def copy(self, destineSubclassLanguageId, sourceSubclassLanguageId):
        """

        Args:
            destineSubclassLanguageId: (string): destineSubclassLanguageId
            sourceSubclassLanguageId: (string): sourceSubclassLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['destineSubclassLanguageId', 'sourceSubclassLanguageId']
        parameters_names_map = {}
        api = '/clusters-management/copy'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def findNames(self, clusterLanguageId, **kwargs):
        """

        Args:
            clusterLanguageId: (string): clusterLanguageId
            maxNames: (integer): maxNames

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/clusters-management/findNames'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def visualize(self, languageId, **kwargs):
        """

        Args:
            className: (string): { "themes", "doctypes", "emotions", "sentiments", "clusters" }
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', '**kwargs']
        parameters_names_map = {}
        api = '/clusters-management/visualize'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
