from edison_dictionary_api import EdisonBaseAPI


class emotionsLanguagesController(EdisonBaseAPI):
    """Emotions languages"""

    _controller_name = "emotionsLanguagesController"

    def add(self, emotionId, languageId):
        """

        Args:
            emotionId: (string): emotionId
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionId', 'languageId']
        parameters_names_map = {}
        api = '/emotions-languages/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, emotionId):
        """

        Args:
            emotionId: (string): emotionId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionId']
        parameters_names_map = {}
        api = '/emotions-languages/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, emotionLanguageId):
        """

        Args:
            emotionLanguageId: (string): emotionLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionLanguageId']
        parameters_names_map = {}
        api = '/emotions-languages/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, emotionLanguageId):
        """

        Args:
            emotionLanguageId: (string): emotionLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionLanguageId']
        parameters_names_map = {}
        api = '/emotions-languages/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
