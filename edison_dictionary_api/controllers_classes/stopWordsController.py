from edison_dictionary_api import EdisonBaseAPI


class stopWordsController(EdisonBaseAPI):
    """Stop words."""

    _controller_name = "stopWordsController"

    def add(self, bankId, word):
        """

        Args:
            bankId: (string): bankId
            word: (string): word

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['bankId', 'word']
        parameters_names_map = {}
        api = '/stop-words/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, bankId):
        """

        Args:
            bankId: (string): bankId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['bankId']
        parameters_names_map = {}
        api = '/stop-words/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, wordId):
        """

        Args:
            wordId: (string): wordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['wordId']
        parameters_names_map = {}
        api = '/stop-words/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, wordId):
        """

        Args:
            wordId: (string): wordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['wordId']
        parameters_names_map = {}
        api = '/stop-words/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
