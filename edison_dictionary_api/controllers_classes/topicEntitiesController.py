from edison_dictionary_api import EdisonBaseAPI


class topicEntitiesController(EdisonBaseAPI):
    """Topic entities."""

    _controller_name = "topicEntitiesController"

    def add(self, alias, popularity, topicId, **kwargs):
        """

        Args:
            alias: (string): alias
            popularity: (integer): popularity
            topicId: (string): topicId
            typeId: (string): typeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alias', 'popularity', 'topicId', '**kwargs']
        parameters_names_map = {}
        api = '/topic-entity/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def addAllSpecified(self, entity, topicId):
        """

        Args:
            entity: (string): entity
            topicId: (string): topicId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entity', 'topicId']
        parameters_names_map = {}
        api = '/topic-entity/addAllSpecified'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def clearOriginalEntityId(self, entityId):
        """

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/topic-entity/clearOriginalEntityId'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def clone(self, entityId, topicId, **kwargs):
        """

        Args:
            deleteSource: (boolean): deleteSource
            entityId: (string): entityId
            popularity: (integer): popularity
            topicId: (string): topicId
            typeId: (string): typeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId', 'topicId', '**kwargs']
        parameters_names_map = {}
        api = '/topic-entity/clone'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def downloadExportFile(self, taskId):
        """

        Args:
            taskId: (string): taskId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['taskId']
        parameters_names_map = {}
        api = '/topic-entity/downloadExportFile'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, entityId, **kwargs):
        """

        Args:
            entityId: (string): entityId
            popularity: (integer): popularity
            typeId: (string): typeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId', '**kwargs']
        parameters_names_map = {}
        api = '/topic-entity/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def editAllSpecified(self, entity, topicId):
        """

        Args:
            entity: (string): entity
            topicId: (string): topicId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entity', 'topicId']
        parameters_names_map = {}
        api = '/topic-entity/editAllSpecified'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def exportTopicEntities(self, languageId, topicTypeId):
        """

        Args:
            languageId: (string): languageId
            topicTypeId: (string): topicTypeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', 'topicTypeId']
        parameters_names_map = {}
        api = '/topic-entity/exportTopicEntities'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def getTopicEntities(self, **kwargs):
        """

        Args:
            includeNames: (boolean): includeNames
            languageId: (string): Language should be * for all languages
            maxEntitiesNumber: (integer): maxEntitiesNumber
            name: (string): Name should be * for retrieving all entities with this topicType
            onlyMainNames: (boolean): onlyMainNames
            sortByPopularity: (boolean): sortByPopularity
            topicTypeId: (string): topicTypeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/topic-entity/getTopicEntities'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def getTopicEntitiesPaged(self, **kwargs):
        """

        Args:
            ascending: (boolean): ascending = true; // false - descending(default)
            includeNames: (boolean): includeNames
            languageId: (string): Language should be * for all languages
            maxEntitiesNumber: (integer): maxEntitiesNumber
            name: (string): Name should be * for retrieving all entities with this topicType
            offset: (integer): offset
            onlyMainNames: (boolean): onlyMainNames
            sortBy: (string):  "", /* Don't sort, default */ "mainName", "popularity" 
            topicTypeId: (string): topicTypeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/topic-entity/getTopicEntitiesPaged'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def importTopicEntities(self, file, languageId, topicTypeId, **kwargs):
        """

        Args:
            file: (file): file
            languageId: (string): languageId
            stopOnError: (boolean): stopOnError
            topicTypeId: (string): topicTypeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['file', 'languageId', 'topicTypeId', '**kwargs']
        parameters_names_map = {}
        api = '/topic-entity/importTopicEntities'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, languageId, name, **kwargs):
        """

        Args:
            languageId: (string): languageId
            maxEntitiesNumber: (integer): maxEntitiesNumber
            name: (string): name
            onlyMainNames: (boolean): onlyMainNames
            topicId: (string): topicId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', 'name', '**kwargs']
        parameters_names_map = {}
        api = '/topic-entity/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def listByAlias(self, alias, topicId, **kwargs):
        """

        Args:
            alias: (string): alias
            maxEntitiesNumber: (integer): maxEntitiesNumber
            topicId: (string): topicId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alias', 'topicId', '**kwargs']
        parameters_names_map = {}
        api = '/topic-entity/listByAlias'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, entityId):
        """

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/topic-entity/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def restore(self, entityId):
        """

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/topic-entity/restore'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, entityId):
        """

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/topic-entity/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def setMainName(self, entityId, **kwargs):
        """

        Args:
            entityId: (string): entityId
            nameId: (string): nameId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId', '**kwargs']
        parameters_names_map = {}
        api = '/topic-entity/setMainName'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
