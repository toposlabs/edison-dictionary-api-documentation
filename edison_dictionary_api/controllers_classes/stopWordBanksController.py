from edison_dictionary_api import EdisonBaseAPI


class stopWordBanksController(EdisonBaseAPI):
    """Banks of the stop words."""

    _controller_name = "stopWordBanksController"

    def add(self, languageId):
        """

        Args:
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId']
        parameters_names_map = {}
        api = '/stop-word-banks/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/stop-word-banks/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, bankId):
        """

        Args:
            bankId: (string): bankId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['bankId']
        parameters_names_map = {}
        api = '/stop-word-banks/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, bankId):
        """

        Args:
            bankId: (string): bankId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['bankId']
        parameters_names_map = {}
        api = '/stop-word-banks/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
