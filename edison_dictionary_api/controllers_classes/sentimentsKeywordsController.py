from edison_dictionary_api import EdisonBaseAPI


class sentimentsKeywordsController(EdisonBaseAPI):
    """Sentiments keywords"""

    _controller_name = "sentimentsKeywordsController"

    def add(self, keyword, sentimentLanguageId, weight):
        """

        Args:
            keyword: (string): keyword
            sentimentLanguageId: (string): sentimentLanguageId
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keyword', 'sentimentLanguageId', 'weight']
        parameters_names_map = {}
        api = '/sentiments-keywords/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, keywordId, weight):
        """

        Args:
            keywordId: (string): keywordId
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId', 'weight']
        parameters_names_map = {}
        api = '/sentiments-keywords/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, sentimentLanguageId, **kwargs):
        """

        Args:
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sentimentLanguageId: (string): sentimentLanguageId
            sortMode: (string): { "name", "weight" }
            sortOrderAscending: (boolean): true = ascending (default); false = descending

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/sentiments-keywords/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def listForAllSentiments(self, **kwargs):
        """

        Args:
            languageId: (string): languageId
            minLevel: (number): minLevel

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/sentiments-keywords/listForAllSentiments'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/sentiments-keywords/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/sentiments-keywords/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
