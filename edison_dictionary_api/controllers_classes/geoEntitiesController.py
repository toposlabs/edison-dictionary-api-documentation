from edison_dictionary_api import EdisonBaseAPI


class geoEntitiesController(EdisonBaseAPI):
    """Geo entities.  """

    _controller_name = "geoEntitiesController"

    def add(self, alias, countryId, latitude, longitude, popularity, **kwargs):
        """

        Args:
            adminCode: (string): admin-code
            alias: (string): alias
            countryId: (string): countryId
            featureCodeId: (string): featureCodeId
            latitude: (number): latitude
            longitude: (number): longitude
            parentId: (string): parentId
            popularity: (integer): popularity
            population: (integer): population

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alias', 'countryId', 'latitude', 'longitude', 'popularity', '**kwargs']
        parameters_names_map = {'adminCode': 'admin-code'}
        api = '/geo-entities/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def addAllSpecified(self, countryId, entity):
        """

        Args:
            countryId: (string): countryId
            entity: (string): entity

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['countryId', 'entity']
        parameters_names_map = {}
        api = '/geo-entities/addAllSpecified'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, entityId, **kwargs):
        """

        Args:
            adminCode: (string): admin-code
            entityId: (string): entityId
            featureCodeId: (string): featureCodeId
            latitude: (number): latitude
            longitude: (number): longitude
            parentId: (string): parentId
            popularity: (integer): popularity
            population: (integer): population

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId', '**kwargs']
        parameters_names_map = {'adminCode': 'admin-code'}
        api = '/geo-entities/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def editAllSpecified(self, countryId, entity):
        """

        Args:
            countryId: (string): countryId
            entity: (string): entity

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['countryId', 'entity']
        parameters_names_map = {}
        api = '/geo-entities/editAllSpecified'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def getGeoEntities(self, languageId, name, **kwargs):
        """

        Args:
            languageId: (string): languageId
            maxEntitiesNumber: (integer): maxEntitiesNumber
            name: (string): name
            onlyDirectChildren: (boolean): onlyDirectChildren
            onlyMainNames: (boolean): onlyMainNames
            parentEntityId: (string): parentEntityId
            sortByPopularity: (boolean): sortByPopularity
            wholeSubTree: (boolean): wholeSubTree

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', 'name', '**kwargs']
        parameters_names_map = {}
        api = '/geo-entities/getGeoEntities'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def getGeoEntitiesPaged(self, languageId, name, **kwargs):
        """

        Args:
            ascending: (boolean): ascending = true; // false - descending(default)
            languageId: (string): languageId
            maxEntitiesNumber: (integer): maxEntitiesNumber
            name: (string): name
            offset: (integer): offset
            onlyDirectChildren: (boolean): onlyDirectChildren
            onlyMainNames: (boolean): onlyMainNames
            parentEntityId: (string): parentEntityId
            sortBy: (string):  "", /* Don't sort, default */ "mainName", "popularity" 
            wholeSubTree: (boolean): wholeSubTree

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', 'name', '**kwargs']
        parameters_names_map = {}
        api = '/geo-entities/getGeoEntitiesPaged'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, languageId, name, **kwargs):
        """

        Args:
            countryId: (string): countryId
            languageId: (string): languageId
            maxEntitiesNumber: (integer): maxEntitiesNumber
            name: (string): name
            onlyMainNames: (boolean): onlyMainNames

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', 'name', '**kwargs']
        parameters_names_map = {}
        api = '/geo-entities/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def listByAlias(self, alias, countryId, **kwargs):
        """

        Args:
            alias: (string): alias
            countryId: (string): countryId
            maxEntitiesNumber: (integer): maxEntitiesNumber

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['alias', 'countryId', '**kwargs']
        parameters_names_map = {}
        api = '/geo-entities/listByAlias'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, entityId):
        """

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/geo-entities/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def restore(self, entityId):
        """

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/geo-entities/restore'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, entityId):
        """

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/geo-entities/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def setMainName(self, entityId, **kwargs):
        """

        Args:
            entityId: (string): entityId
            nameId: (string): nameId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId', '**kwargs']
        parameters_names_map = {}
        api = '/geo-entities/setMainName'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
