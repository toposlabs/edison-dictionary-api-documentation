from edison_dictionary_api import EdisonBaseAPI


class searchController(EdisonBaseAPI):
    """Search"""

    _controller_name = "searchController"

    def processFile(self, file, **kwargs):
        """

        Args:
            file: (file): file
            filterFields: (string): filterFields
            idfScoreMin: (number): idfScoreMin
            inputLanguageId: (string): inputLanguageId
            logging: (boolean): logging
            minRelevance: (integer): min-relevance
            typeId: (string): typeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['file', '**kwargs']
        parameters_names_map = {'minRelevance': 'min-relevance'}
        api = '/search/processFile'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def processText(self, text, **kwargs):
        """

        Args:
            filterFields: (string): filterFields
            idfScoreMin: (number): idfScoreMin
            inputLanguageId: (string): inputLanguageId
            logging: (boolean): logging
            minRelevance: (integer): min-relevance
            text: (string): text
            typeId: (string): typeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['text', '**kwargs']
        parameters_names_map = {'minRelevance': 'min-relevance'}
        api = '/search/processText'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
