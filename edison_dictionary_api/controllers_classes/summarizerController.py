from edison_dictionary_api import EdisonBaseAPI


class summarizerController(EdisonBaseAPI):
    """Summarize text down to N sentences."""

    _controller_name = "summarizerController"

    def summarize(self, maxSentences, text, **kwargs):
        """

        Args:
            inputLanguageId: (string): inputLanguageId
            maxSentences: (integer): maxSentences
            text: (string): text

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['maxSentences', 'text', '**kwargs']
        parameters_names_map = {}
        api = '/summarize'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
