from edison_dictionary_api import EdisonBaseAPI


class editorsController(EdisonBaseAPI):
    """Editor - is someone who can edit data in the database. """

    _controller_name = "editorsController"

    def assignAll(self, dictionariesAssignment):
        """

        Args:
            dictionariesAssignment: (string): dictionariesAssignment

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['dictionariesAssignment']
        parameters_names_map = {}
        api = '/editors/assignAll'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def assignCountry(self, dictionaryId, editorId, **kwargs):
        """

        Args:
            assign: (boolean): assign
            dictionaryId: (string): dictionaryId
            editorId: (string): editorId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['dictionaryId', 'editorId', '**kwargs']
        parameters_names_map = {}
        api = '/editors/assignCountry'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def assignTopic(self, dictionaryId, editorId, **kwargs):
        """

        Args:
            assign: (boolean): assign
            dictionaryId: (string): dictionaryId
            editorId: (string): editorId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['dictionaryId', 'editorId', '**kwargs']
        parameters_names_map = {}
        api = '/editors/assignTopic'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def countriesList(self, **kwargs):
        """

        Args:
            editorId: (string): editorId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/editors/countriesList'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/editors/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def listAll(self, editorId):
        """

        Args:
            editorId: (string): editorId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['editorId']
        parameters_names_map = {}
        api = '/editors/listAll'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def topicsList(self, **kwargs):
        """

        Args:
            editorId: (string): editorId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/editors/topicsList'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
