from edison_dictionary_api import EdisonBaseAPI


class inputLanguagesController(EdisonBaseAPI):
    """Supported input languages."""

    _controller_name = "inputLanguagesController"

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/input-languages/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, inputLanguageId):
        """

        Args:
            inputLanguageId: (string): inputLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['inputLanguageId']
        parameters_names_map = {}
        api = '/input-languages/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
