from edison_dictionary_api import EdisonBaseAPI


class continentsController(EdisonBaseAPI):
    """Continents."""

    _controller_name = "continentsController"

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/continents/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, continentId):
        """

        Args:
            continentId: (string): continentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['continentId']
        parameters_names_map = {}
        api = '/continents/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
