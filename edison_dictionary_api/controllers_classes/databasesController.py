from edison_dictionary_api import EdisonBaseAPI


class databasesController(EdisonBaseAPI):
    """Databases."""

    _controller_name = "databasesController"

    def backup(self, **kwargs):
        """

        Args:
            incremental: (boolean): incremental
            removeFolder: (boolean): removeFolder
            zip: (boolean): zip

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/databases/backup'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def clusterArchive(self, clusterLanguageId):
        """

        Args:
            clusterLanguageId: (string): clusterLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterLanguageId']
        parameters_names_map = {}
        api = '/databases/clusterArchive'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def doctypeArchive(self, doctypeLanguageId):
        """

        Args:
            doctypeLanguageId: (string): doctypeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeLanguageId']
        parameters_names_map = {}
        api = '/databases/doctypeArchive'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def downloadClusterDocuments(self, clusterLanguageId):
        """

        Args:
            clusterLanguageId: (string): clusterLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterLanguageId']
        parameters_names_map = {}
        api = '/databases/downloadClusterDocuments'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def downloadDoctypeDocuments(self, doctypeLanguageId):
        """

        Args:
            doctypeLanguageId: (string): doctypeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeLanguageId']
        parameters_names_map = {}
        api = '/databases/downloadDoctypeDocuments'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def downloadEmotionDocuments(self, emotionLanguageId):
        """

        Args:
            emotionLanguageId: (string): emotionLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionLanguageId']
        parameters_names_map = {}
        api = '/databases/downloadEmotionDocuments'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def downloadSentimentDocuments(self, sentimentLanguageId):
        """

        Args:
            sentimentLanguageId: (string): sentimentLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentLanguageId']
        parameters_names_map = {}
        api = '/databases/downloadSentimentDocuments'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def downloadThemeDocuments(self, themeLanguageId):
        """

        Args:
            themeLanguageId: (string): themeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeLanguageId']
        parameters_names_map = {}
        api = '/databases/downloadThemeDocuments'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def downloadTopicTypeDocuments(self, topicTypeLanguageId):
        """

        Args:
            topicTypeLanguageId: (string): topicTypeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['topicTypeLanguageId']
        parameters_names_map = {}
        api = '/databases/downloadTopicTypeDocuments'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def emotionArchive(self, emotionLanguageId):
        """

        Args:
            emotionLanguageId: (string): emotionLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionLanguageId']
        parameters_names_map = {}
        api = '/databases/emotionArchive'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def sentimentArchive(self, sentimentLanguageId):
        """

        Args:
            sentimentLanguageId: (string): sentimentLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentLanguageId']
        parameters_names_map = {}
        api = '/databases/sentimentArchive'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def themeArchive(self, themeLanguageId):
        """

        Args:
            themeLanguageId: (string): themeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeLanguageId']
        parameters_names_map = {}
        api = '/databases/themeArchive'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def topicTypeArchive(self, topicTypeLanguageId):
        """

        Args:
            topicTypeLanguageId: (string): topicTypeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['topicTypeLanguageId']
        parameters_names_map = {}
        api = '/databases/topicTypeArchive'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def updateBinaries(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/databases/updateBinaries'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def updateDictionaries(self, **kwargs):
        """

        Args:
            dictionaryId: (string): dictionaryId
            recalculateDocumentsVectors: (boolean): recalculateDocumentsVectors

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/databases/updateDictionaries'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def updateDoc2vec(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/databases/updateDoc2vec'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def updateDoctypes(self, **kwargs):
        """

        Args:
            recalculateDocumentsVectors: (boolean): recalculateDocumentsVectors
            subclassId: (string): subclassId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/databases/updateDoctypes'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def updateEmotions(self, **kwargs):
        """

        Args:
            recalculateDocumentsVectors: (boolean): recalculateDocumentsVectors
            subclassId: (string): subclassId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/databases/updateEmotions'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def updateIdfModel(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/databases/updateIdfModel'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def updateSentiment(self, **kwargs):
        """

        Args:
            recalculateDocumentsVectors: (boolean): recalculateDocumentsVectors
            subclassId: (string): subclassId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/databases/updateSentiment'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def updateThemes(self, **kwargs):
        """

        Args:
            recalculateDocumentsVectors: (boolean): recalculateDocumentsVectors
            subclassId: (string): subclassId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/databases/updateThemes'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
