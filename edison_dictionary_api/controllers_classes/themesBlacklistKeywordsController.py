from edison_dictionary_api import EdisonBaseAPI


class themesBlacklistKeywordsController(EdisonBaseAPI):
    """Keywords from a blacklist of a themes"""

    _controller_name = "themesBlacklistKeywordsController"

    def add(self, keyword, themeLanguageId):
        """

        Args:
            keyword: (string): keyword
            themeLanguageId: (string): themeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keyword', 'themeLanguageId']
        parameters_names_map = {}
        api = '/themes-blacklist-keywords/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, themeLanguageId, **kwargs):
        """

        Args:
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sortOrderAscending: (boolean): true = ascending (default); false = descending
            themeLanguageId: (string): themeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/themes-blacklist-keywords/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/themes-blacklist-keywords/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/themes-blacklist-keywords/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
