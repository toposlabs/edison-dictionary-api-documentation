from edison_dictionary_api import EdisonBaseAPI


class clustersDocumentsController(EdisonBaseAPI):
    """Clusters documents"""

    _controller_name = "clustersDocumentsController"

    def add(self, clusterLanguageId, text, **kwargs):
        """

        Args:
            clusterLanguageId: (string): clusterLanguageId
            text: (string): text
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterLanguageId', 'text', '**kwargs']
        parameters_names_map = {}
        api = '/clusters-documents/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def addFile(self, clusterLanguageId, files, **kwargs):
        """Supported file formats: https://tika.apache.org/1.13/formats.html

        Args:
            clusterLanguageId: (string): clusterLanguageId
            files: (array): files
            splitByLines: (boolean): splitByLines

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterLanguageId', 'files', '**kwargs']
        parameters_names_map = {}
        api = '/clusters-documents/addFile'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, clusterDocumentId, **kwargs):
        """

        Args:
            clusterDocumentId: (string): clusterDocumentId
            text: (string): text
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterDocumentId', '**kwargs']
        parameters_names_map = {}
        api = '/clusters-documents/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, clusterLanguageId, **kwargs):
        """

        Args:
            clusterLanguageId: (string): clusterLanguageId
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sortMode: (string): { "weight", "proximity", "doc2vec", "keywords", "entities" }
            sortOrderAscending: (boolean): true = ascending (default); false = descending

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/clusters-documents/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, clusterDocumentId):
        """

        Args:
            clusterDocumentId: (string): clusterDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterDocumentId']
        parameters_names_map = {}
        api = '/clusters-documents/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, clusterDocumentId):
        """

        Args:
            clusterDocumentId: (string): clusterDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterDocumentId']
        parameters_names_map = {}
        api = '/clusters-documents/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
