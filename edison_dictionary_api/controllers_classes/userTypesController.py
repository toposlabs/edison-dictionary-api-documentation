from edison_dictionary_api import EdisonBaseAPI


class userTypesController(EdisonBaseAPI):
    """User types."""

    _controller_name = "userTypesController"

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/user-types/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, id):
        """

        Args:
            id: (string): id

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id']
        parameters_names_map = {}
        api = '/user-types/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
