from edison_dictionary_api import EdisonBaseAPI


class clustersController(EdisonBaseAPI):
    """Cluster"""

    _controller_name = "clustersController"

    def add(self, code, **kwargs):
        """

        Args:
            code: (string): code
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['code', '**kwargs']
        parameters_names_map = {}
        api = '/clusters/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/clusters/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, clusterId):
        """

        Args:
            clusterId: (string): clusterId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterId']
        parameters_names_map = {}
        api = '/clusters/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, clusterId):
        """

        Args:
            clusterId: (string): clusterId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterId']
        parameters_names_map = {}
        api = '/clusters/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
