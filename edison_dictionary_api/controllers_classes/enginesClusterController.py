from edison_dictionary_api import EdisonBaseAPI


class enginesClusterController(EdisonBaseAPI):
    """Engines cluster"""

    _controller_name = "enginesClusterController"

    def clusterStatus(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/engines/cluster/status'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def clusterWorkers(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/engines/cluster/workers'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
