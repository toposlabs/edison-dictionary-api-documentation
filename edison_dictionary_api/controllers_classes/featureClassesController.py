from edison_dictionary_api import EdisonBaseAPI


class featureClassesController(EdisonBaseAPI):
    """Features classes."""

    _controller_name = "featureClassesController"

    def add(self, code, **kwargs):
        """

        Args:
            code: (string): code
            description: (string): description

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['code', '**kwargs']
        parameters_names_map = {}
        api = '/feature-classes/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, featureClassId, **kwargs):
        """

        Args:
            description: (string): description
            featureClassId: (string): featureClassId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['featureClassId', '**kwargs']
        parameters_names_map = {}
        api = '/feature-classes/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/feature-classes/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, featureClassId):
        """

        Args:
            featureClassId: (string): featureClassId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['featureClassId']
        parameters_names_map = {}
        api = '/feature-classes/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, featureClassId):
        """

        Args:
            featureClassId: (string): featureClassId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['featureClassId']
        parameters_names_map = {}
        api = '/feature-classes/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
