from edison_dictionary_api import EdisonBaseAPI


class doctypesDocumentsController(EdisonBaseAPI):
    """Doc-types documents"""

    _controller_name = "doctypesDocumentsController"

    def add(self, doctypeLanguageId, text, **kwargs):
        """

        Args:
            doctypeLanguageId: (string): doctypeLanguageId
            text: (string): text
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeLanguageId', 'text', '**kwargs']
        parameters_names_map = {}
        api = '/doc-types-documents/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def addFile(self, doctypeLanguageId, files, **kwargs):
        """Supported file formats: https://tika.apache.org/1.13/formats.html

        Args:
            doctypeLanguageId: (string): doctypeLanguageId
            files: (array): files
            splitByLines: (boolean): splitByLines

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeLanguageId', 'files', '**kwargs']
        parameters_names_map = {}
        api = '/doc-types-documents/addFile'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, doctypeDocumentId, **kwargs):
        """

        Args:
            doctypeDocumentId: (string): doctypeDocumentId
            text: (string): text
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeDocumentId', '**kwargs']
        parameters_names_map = {}
        api = '/doc-types-documents/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, doctypeLanguageId, **kwargs):
        """

        Args:
            doctypeLanguageId: (string): doctypeLanguageId
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sortMode: (string): { "weight", "proximity", "doc2vec", "keywords", "entities"}
            sortOrderAscending: (boolean): true = ascending (default); false = descending

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/doc-types-documents/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, doctypeDocumentId):
        """

        Args:
            doctypeDocumentId: (string): doctypeDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeDocumentId']
        parameters_names_map = {}
        api = '/doc-types-documents/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, doctypeDocumentId):
        """

        Args:
            doctypeDocumentId: (string): doctypeDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeDocumentId']
        parameters_names_map = {}
        api = '/doc-types-documents/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
