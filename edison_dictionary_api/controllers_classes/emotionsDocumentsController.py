from edison_dictionary_api import EdisonBaseAPI


class emotionsDocumentsController(EdisonBaseAPI):
    """Emotions documents"""

    _controller_name = "emotionsDocumentsController"

    def add(self, emotionLanguageId, text, **kwargs):
        """

        Args:
            emotionLanguageId: (string): emotionLanguageId
            text: (string): text
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionLanguageId', 'text', '**kwargs']
        parameters_names_map = {}
        api = '/emotions-documents/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def addFile(self, emotionLanguageId, files, **kwargs):
        """Supported file formats: https://tika.apache.org/1.13/formats.html

        Args:
            emotionLanguageId: (string): emotionLanguageId
            files: (array): files
            splitByLines: (boolean): splitByLines

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionLanguageId', 'files', '**kwargs']
        parameters_names_map = {}
        api = '/emotions-documents/addFile'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, emotionDocumentId, **kwargs):
        """

        Args:
            emotionDocumentId: (string): emotionDocumentId
            text: (string): text
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionDocumentId', '**kwargs']
        parameters_names_map = {}
        api = '/emotions-documents/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, emotionLanguageId, **kwargs):
        """

        Args:
            emotionLanguageId: (string): emotionLanguageId
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sortMode: (string): { "weight", "proximity", "doc2vec", "keywords", "entities" }
            sortOrderAscending: (boolean): true = ascending (default); false = descending

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/emotions-documents/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, emotionDocumentId):
        """

        Args:
            emotionDocumentId: (string): emotionDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionDocumentId']
        parameters_names_map = {}
        api = '/emotions-documents/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, emotionDocumentId):
        """

        Args:
            emotionDocumentId: (string): emotionDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionDocumentId']
        parameters_names_map = {}
        api = '/emotions-documents/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
