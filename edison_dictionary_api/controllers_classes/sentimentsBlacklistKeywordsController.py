from edison_dictionary_api import EdisonBaseAPI


class sentimentsBlacklistKeywordsController(EdisonBaseAPI):
    """Keywords from a blacklist of a sentiments"""

    _controller_name = "sentimentsBlacklistKeywordsController"

    def add(self, keyword, sentimentLanguageId):
        """

        Args:
            keyword: (string): keyword
            sentimentLanguageId: (string): sentimentLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keyword', 'sentimentLanguageId']
        parameters_names_map = {}
        api = '/sentiments-blacklist-keywords/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, sentimentLanguageId, **kwargs):
        """

        Args:
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sentimentLanguageId: (string): sentimentLanguageId
            sortOrderAscending: (boolean): true = ascending (default); false = descending

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/sentiments-blacklist-keywords/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/sentiments-blacklist-keywords/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/sentiments-blacklist-keywords/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
