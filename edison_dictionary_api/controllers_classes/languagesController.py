from edison_dictionary_api import EdisonBaseAPI


class languagesController(EdisonBaseAPI):
    """Supported languages."""

    _controller_name = "languagesController"

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/languages/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, languageId):
        """

        Args:
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId']
        parameters_names_map = {}
        api = '/languages/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
