from edison_dictionary_api import EdisonBaseAPI


class doctypesLanguagesController(EdisonBaseAPI):
    """Doc-types languages"""

    _controller_name = "doctypesLanguagesController"

    def add(self, doctypeId, languageId):
        """

        Args:
            doctypeId: (string): doctypeId
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeId', 'languageId']
        parameters_names_map = {}
        api = '/doc-types-languages/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, doctypeId):
        """

        Args:
            doctypeId: (string): doctypeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeId']
        parameters_names_map = {}
        api = '/doc-types-languages/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, doctypeLanguageId):
        """

        Args:
            doctypeLanguageId: (string): doctypeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeLanguageId']
        parameters_names_map = {}
        api = '/doc-types-languages/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, doctypeLanguageId):
        """

        Args:
            doctypeLanguageId: (string): doctypeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeLanguageId']
        parameters_names_map = {}
        api = '/doc-types-languages/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
