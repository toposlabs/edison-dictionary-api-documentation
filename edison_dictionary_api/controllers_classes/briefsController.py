from edison_dictionary_api import EdisonBaseAPI


class briefsController(EdisonBaseAPI):
    """Briefs."""

    _controller_name = "briefsController"

    def add(self, brief, entityId, languageId):
        """

        Args:
            brief: (string): brief
            entityId: (string): entityId
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['brief', 'entityId', 'languageId']
        parameters_names_map = {}
        api = '/briefs/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, brief, briefId):
        """

        Args:
            brief: (string): brief
            briefId: (string): briefId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['brief', 'briefId']
        parameters_names_map = {}
        api = '/briefs/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, entityId):
        """

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/briefs/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, briefId):
        """

        Args:
            briefId: (string): briefId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['briefId']
        parameters_names_map = {}
        api = '/briefs/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, briefId):
        """

        Args:
            briefId: (string): briefId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['briefId']
        parameters_names_map = {}
        api = '/briefs/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
