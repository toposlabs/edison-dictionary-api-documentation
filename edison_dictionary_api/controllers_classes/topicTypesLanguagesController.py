from edison_dictionary_api import EdisonBaseAPI


class topicTypesLanguagesController(EdisonBaseAPI):
    """Topic-types languages"""

    _controller_name = "topicTypesLanguagesController"

    def add(self, languageId, topicTypeId):
        """

        Args:
            languageId: (string): languageId
            topicTypeId: (string): topicTypeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', 'topicTypeId']
        parameters_names_map = {}
        api = '/topic-types-languages/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, topicTypeId):
        """

        Args:
            topicTypeId: (string): topicTypeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['topicTypeId']
        parameters_names_map = {}
        api = '/topic-types-languages/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, topicTypeLanguageId):
        """

        Args:
            topicTypeLanguageId: (string): topicTypeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['topicTypeLanguageId']
        parameters_names_map = {}
        api = '/topic-types-languages/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, topicTypeLanguageId):
        """

        Args:
            topicTypeLanguageId: (string): topicTypeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['topicTypeLanguageId']
        parameters_names_map = {}
        api = '/topic-types-languages/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
