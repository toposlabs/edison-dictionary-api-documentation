from edison_dictionary_api import EdisonBaseAPI


class testClassesController(EdisonBaseAPI):
    """Test classes"""

    _controller_name = "testClassesController"

    def runClass(self, languageId, **kwargs):
        """

        Args:
            className: (string): { "themes", "doctypes", "emotions", "sentiments", "topics" }
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', '**kwargs']
        parameters_names_map = {}
        api = '/test-classes/runClass'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def runDocument(self, **kwargs):
        """

        Args:
            documentId: (string): id of some of documents { theme, doc-type, emotion, sentiment, topic-entity-brief, topic-type }

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/test-classes/runDocument'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def runSubclass(self, languageId, **kwargs):
        """

        Args:
            languageId: (string): languageId
            subclassId: (string): id of some of { theme, doc-type, emotion, sentiment, topic, topic-type }

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', '**kwargs']
        parameters_names_map = {}
        api = '/test-classes/runSubclass'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
