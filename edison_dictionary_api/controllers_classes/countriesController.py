from edison_dictionary_api import EdisonBaseAPI


class countriesController(EdisonBaseAPI):
    """Countries."""

    _controller_name = "countriesController"

    def add(self, code, name, **kwargs):
        """

        Args:
            capital: (string): capital
            code: (string): code
            continentId: (string): continentId
            currencyCode: (string): currency-code
            currencyName: (string): currency-name
            name: (string): name
            phoneCode: (string): phone-code
            postalCodeFormat: (string): postal-code-format
            postalCodeRegExp: (string): postal-code-reg-exp
            topLevelDomain: (string): top-level-domain

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['code', 'name', '**kwargs']
        parameters_names_map = {'currencyCode': 'currency-code', 'currencyName': 'currency-name', 'phoneCode': 'phone-code', 'postalCodeFormat': 'postal-code-format', 'postalCodeRegExp': 'postal-code-reg-exp', 'topLevelDomain': 'top-level-domain'}
        api = '/countries/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, id, **kwargs):
        """

        Args:
            capital: (string): capital
            continentId: (string): continentId
            currencyCode: (string): currency-code
            currencyName: (string): currency-name
            id: (string): id
            name: (string): name
            phoneCode: (string): phone-code
            postalCodeFormat: (string): postal-code-format
            postalCodeRegExp: (string): postal-code-reg-exp
            topLevelDomain: (string): top-level-domain

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id', '**kwargs']
        parameters_names_map = {'currencyCode': 'currency-code', 'currencyName': 'currency-name', 'phoneCode': 'phone-code', 'postalCodeFormat': 'postal-code-format', 'postalCodeRegExp': 'postal-code-reg-exp', 'topLevelDomain': 'top-level-domain'}
        api = '/countries/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/countries/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, id):
        """

        Args:
            id: (string): id

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id']
        parameters_names_map = {}
        api = '/countries/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, id):
        """

        Args:
            id: (string): id

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id']
        parameters_names_map = {}
        api = '/countries/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
