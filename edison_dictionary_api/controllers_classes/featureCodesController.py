from edison_dictionary_api import EdisonBaseAPI


class featureCodesController(EdisonBaseAPI):
    """Features-codes."""

    _controller_name = "featureCodesController"

    def add(self, featureClassId, name, subCode, **kwargs):
        """

        Args:
            description: (string): description
            featureClassId: (string): featureClassId
            name: (string): name
            subCode: (string): subCode

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['featureClassId', 'name', 'subCode', '**kwargs']
        parameters_names_map = {}
        api = '/feature-codes/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, featureCodeId, **kwargs):
        """

        Args:
            description: (string): description
            featureCodeId: (string): featureCodeId
            name: (string): name

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['featureCodeId', '**kwargs']
        parameters_names_map = {}
        api = '/feature-codes/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, **kwargs):
        """

        Args:
            featureClassId: (string): featureClassId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/feature-codes/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, featureCodeId):
        """

        Args:
            featureCodeId: (string): featureCodeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['featureCodeId']
        parameters_names_map = {}
        api = '/feature-codes/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, featureCodeId):
        """

        Args:
            featureCodeId: (string): featureCodeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['featureCodeId']
        parameters_names_map = {}
        api = '/feature-codes/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
