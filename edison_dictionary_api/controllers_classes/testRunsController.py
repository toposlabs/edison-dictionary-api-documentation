from edison_dictionary_api import EdisonBaseAPI


class testRunsController(EdisonBaseAPI):
    """Test runs"""

    _controller_name = "testRunsController"

    def export(self, testDocumentId):
        """

        Args:
            testDocumentId: (string): testDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testDocumentId']
        parameters_names_map = {}
        api = '/test-runs/export'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, testDocumentId, **kwargs):
        """

        Args:
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            testDocumentId: (string): testDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testDocumentId', '**kwargs']
        parameters_names_map = {}
        api = '/test-runs/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, testRunId):
        """

        Args:
            testRunId: (string): testRunId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testRunId']
        parameters_names_map = {}
        api = '/test-runs/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, testRunId):
        """

        Args:
            testRunId: (string): testRunId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testRunId']
        parameters_names_map = {}
        api = '/test-runs/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
