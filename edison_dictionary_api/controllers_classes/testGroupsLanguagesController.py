from edison_dictionary_api import EdisonBaseAPI


class testGroupsLanguagesController(EdisonBaseAPI):
    """Test groups languages"""

    _controller_name = "testGroupsLanguagesController"

    def add(self, languageId, testGroupId):
        """

        Args:
            languageId: (string): languageId
            testGroupId: (string): testGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', 'testGroupId']
        parameters_names_map = {}
        api = '/test-groups-languages/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, testGroupId):
        """

        Args:
            testGroupId: (string): testGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testGroupId']
        parameters_names_map = {}
        api = '/test-groups-languages/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, testGroupLanguageId):
        """

        Args:
            testGroupLanguageId: (string): testGroupLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testGroupLanguageId']
        parameters_names_map = {}
        api = '/test-groups-languages/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, testGroupLanguageId):
        """

        Args:
            testGroupLanguageId: (string): testGroupLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testGroupLanguageId']
        parameters_names_map = {}
        api = '/test-groups-languages/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def run(self, testGroupLanguageId, **kwargs):
        """

        Args:
            includeChanged: (boolean): includeChanged
            includeMissing: (boolean): includeMissing
            includeUnchanged: (boolean): includeUnchanged
            includeUnexpected: (boolean): includeUnexpected
            testGroupLanguageId: (string): testGroupLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testGroupLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/test-groups-languages/run'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
