from edison_dictionary_api import EdisonBaseAPI


class testDocumentsController(EdisonBaseAPI):
    """Test documents"""

    _controller_name = "testDocumentsController"

    def add(self, testGroupLanguageId, text):
        """

        Args:
            testGroupLanguageId: (string): testGroupLanguageId
            text: (string): text

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testGroupLanguageId', 'text']
        parameters_names_map = {}
        api = '/test-documents/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def addFile(self, files, testGroupLanguageId):
        """

        Args:
            files: (array): files
            testGroupLanguageId: (string): testGroupLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['files', 'testGroupLanguageId']
        parameters_names_map = {}
        api = '/test-documents/addFile'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, desiredResult, testDocumentId):
        """

        Args:
            desiredResult: (string): desiredResult
            testDocumentId: (string): testDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['desiredResult', 'testDocumentId']
        parameters_names_map = {}
        api = '/test-documents/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, testGroupLanguageId, **kwargs):
        """

        Args:
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            testGroupLanguageId: (string): testGroupLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testGroupLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/test-documents/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, testDocumentId):
        """

        Args:
            testDocumentId: (string): testDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testDocumentId']
        parameters_names_map = {}
        api = '/test-documents/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, testDocumentId):
        """

        Args:
            testDocumentId: (string): testDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testDocumentId']
        parameters_names_map = {}
        api = '/test-documents/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
