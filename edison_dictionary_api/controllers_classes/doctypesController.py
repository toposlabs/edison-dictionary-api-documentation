from edison_dictionary_api import EdisonBaseAPI


class doctypesController(EdisonBaseAPI):
    """Doc-types"""

    _controller_name = "doctypesController"

    def add(self, code, **kwargs):
        """

        Args:
            code: (string): code
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['code', '**kwargs']
        parameters_names_map = {}
        api = '/doc-types/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/doc-types/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, doctypeId):
        """

        Args:
            doctypeId: (string): doctypeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeId']
        parameters_names_map = {}
        api = '/doc-types/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, doctypeId):
        """

        Args:
            doctypeId: (string): doctypeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['doctypeId']
        parameters_names_map = {}
        api = '/doc-types/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
