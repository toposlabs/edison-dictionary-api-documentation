from edison_dictionary_api import EdisonBaseAPI


class sentimentsDocumentsController(EdisonBaseAPI):
    """Sentiments documents"""

    _controller_name = "sentimentsDocumentsController"

    def add(self, sentimentLanguageId, text, **kwargs):
        """

        Args:
            sentimentLanguageId: (string): sentimentLanguageId
            text: (string): text
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentLanguageId', 'text', '**kwargs']
        parameters_names_map = {}
        api = '/sentiments-documents/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def addFile(self, files, sentimentLanguageId, **kwargs):
        """Supported file formats: https://tika.apache.org/1.13/formats.html

        Args:
            files: (array): files
            sentimentLanguageId: (string): sentimentLanguageId
            splitByLines: (boolean): splitByLines

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['files', 'sentimentLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/sentiments-documents/addFile'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, sentimentDocumentId, **kwargs):
        """

        Args:
            sentimentDocumentId: (string): sentimentDocumentId
            text: (string): text
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentDocumentId', '**kwargs']
        parameters_names_map = {}
        api = '/sentiments-documents/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, sentimentLanguageId, **kwargs):
        """

        Args:
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sentimentLanguageId: (string): sentimentLanguageId
            sortMode: (string): { "weight", "proximity", "doc2vec", "keywords", "entities" }
            sortOrderAscending: (boolean): true = ascending (default); false = descending

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/sentiments-documents/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, sentimentDocumentId):
        """

        Args:
            sentimentDocumentId: (string): sentimentDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentDocumentId']
        parameters_names_map = {}
        api = '/sentiments-documents/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, sentimentDocumentId):
        """

        Args:
            sentimentDocumentId: (string): sentimentDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentDocumentId']
        parameters_names_map = {}
        api = '/sentiments-documents/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
