from edison_dictionary_api import EdisonBaseAPI


class topicsController(EdisonBaseAPI):
    """Topics"""

    _controller_name = "topicsController"

    def add(self, code):
        """

        Args:
            code: (string): code

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['code']
        parameters_names_map = {}
        api = '/topics/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/topics/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, topicId):
        """

        Args:
            topicId: (string): topicId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['topicId']
        parameters_names_map = {}
        api = '/topics/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, topicId):
        """

        Args:
            topicId: (string): topicId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['topicId']
        parameters_names_map = {}
        api = '/topics/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
