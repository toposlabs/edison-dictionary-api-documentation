from edison_dictionary_api import EdisonBaseAPI


class themesController(EdisonBaseAPI):
    """Themes"""

    _controller_name = "themesController"

    def add(self, code, **kwargs):
        """

        Args:
            code: (string): code
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['code', '**kwargs']
        parameters_names_map = {}
        api = '/themes/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/themes/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, themeId):
        """

        Args:
            themeId: (string): themeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeId']
        parameters_names_map = {}
        api = '/themes/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, themeId):
        """

        Args:
            themeId: (string): themeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeId']
        parameters_names_map = {}
        api = '/themes/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
