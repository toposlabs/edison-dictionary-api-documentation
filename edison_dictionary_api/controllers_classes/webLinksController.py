from edison_dictionary_api import EdisonBaseAPI


class webLinksController(EdisonBaseAPI):
    """Web links. """

    _controller_name = "webLinksController"

    def add(self, entityId, webLink):
        """

        Args:
            entityId: (string): entityId
            webLink: (string): web-link

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId', 'webLink']
        parameters_names_map = {'webLink': 'web-link'}
        api = '/web-links/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, entityId):
        """

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/web-links/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, webLinkId):
        """

        Args:
            webLinkId: (string): webLinkId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['webLinkId']
        parameters_names_map = {}
        api = '/web-links/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, webLinkId):
        """

        Args:
            webLinkId: (string): webLinkId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['webLinkId']
        parameters_names_map = {}
        api = '/web-links/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
