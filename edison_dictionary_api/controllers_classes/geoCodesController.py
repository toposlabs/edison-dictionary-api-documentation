from edison_dictionary_api import EdisonBaseAPI


class geoCodesController(EdisonBaseAPI):
    """Phone and postal codes."""

    _controller_name = "geoCodesController"

    def add(self, code, entityId, typeId):
        """

        Args:
            code: (string): code
            entityId: (string): entityId
            typeId: (string): typeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['code', 'entityId', 'typeId']
        parameters_names_map = {}
        api = '/geo-codes/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, entityId, typeId):
        """

        Args:
            entityId: (string): entityId
            typeId: (string): typeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId', 'typeId']
        parameters_names_map = {}
        api = '/geo-codes/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, codeId):
        """

        Args:
            codeId: (string): codeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['codeId']
        parameters_names_map = {}
        api = '/geo-codes/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, codeId):
        """

        Args:
            codeId: (string): codeId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['codeId']
        parameters_names_map = {}
        api = '/geo-codes/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def typesList(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/geo-codes/typesList'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
