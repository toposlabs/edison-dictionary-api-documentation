from edison_dictionary_api import EdisonBaseAPI


class themesDocumentsController(EdisonBaseAPI):
    """Themes documents"""

    _controller_name = "themesDocumentsController"

    def add(self, text, themeLanguageId, **kwargs):
        """

        Args:
            text: (string): text
            themeLanguageId: (string): themeLanguageId
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['text', 'themeLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/themes-documents/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def addFile(self, files, themeLanguageId, **kwargs):
        """Supported file formats: https://tika.apache.org/1.13/formats.html

        Args:
            files: (array): files
            splitByLines: (boolean): splitByLines
            themeLanguageId: (string): themeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['files', 'themeLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/themes-documents/addFile'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, themeDocumentId, **kwargs):
        """

        Args:
            text: (string): text
            themeDocumentId: (string): themeDocumentId
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeDocumentId', '**kwargs']
        parameters_names_map = {}
        api = '/themes-documents/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, themeLanguageId, **kwargs):
        """

        Args:
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sortMode: (string): { "weight", "proximity", "doc2vec", "keywords", "entities" }
            sortOrderAscending: (boolean): true = ascending (default); false = descending
            themeLanguageId: (string): themeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/themes-documents/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, themeDocumentId):
        """

        Args:
            themeDocumentId: (string): themeDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeDocumentId']
        parameters_names_map = {}
        api = '/themes-documents/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, themeDocumentId):
        """

        Args:
            themeDocumentId: (string): themeDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeDocumentId']
        parameters_names_map = {}
        api = '/themes-documents/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
