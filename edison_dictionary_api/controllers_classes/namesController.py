from edison_dictionary_api import EdisonBaseAPI


class namesController(EdisonBaseAPI):
    """Names. """

    _controller_name = "namesController"

    def add(self, entityId, languageId, name):
        """

        Args:
            entityId: (string): entityId
            languageId: (string): languageId
            name: (string): name

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId', 'languageId', 'name']
        parameters_names_map = {}
        api = '/names/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, entityId):
        """

        Args:
            entityId: (string): entityId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['entityId']
        parameters_names_map = {}
        api = '/names/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, nameId):
        """

        Args:
            nameId: (string): nameId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['nameId']
        parameters_names_map = {}
        api = '/names/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, nameId):
        """

        Args:
            nameId: (string): nameId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['nameId']
        parameters_names_map = {}
        api = '/names/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
