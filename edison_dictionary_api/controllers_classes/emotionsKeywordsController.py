from edison_dictionary_api import EdisonBaseAPI


class emotionsKeywordsController(EdisonBaseAPI):
    """Emotions keywords"""

    _controller_name = "emotionsKeywordsController"

    def add(self, emotionLanguageId, keyword, weight):
        """

        Args:
            emotionLanguageId: (string): emotionLanguageId
            keyword: (string): keyword
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionLanguageId', 'keyword', 'weight']
        parameters_names_map = {}
        api = '/emotions-keywords/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, keywordId, weight):
        """

        Args:
            keywordId: (string): keywordId
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId', 'weight']
        parameters_names_map = {}
        api = '/emotions-keywords/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, emotionLanguageId, **kwargs):
        """

        Args:
            emotionLanguageId: (string): emotionLanguageId
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sortMode: (string): { "name", "weight" }
            sortOrderAscending: (boolean): true = ascending (default); false = descending

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['emotionLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/emotions-keywords/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def listForAllEmotions(self, **kwargs):
        """

        Args:
            languageId: (string): languageId
            minLevel: (number): minLevel

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/emotions-keywords/listForAllEmotions'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/emotions-keywords/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/emotions-keywords/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
