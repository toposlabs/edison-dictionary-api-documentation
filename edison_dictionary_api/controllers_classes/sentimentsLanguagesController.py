from edison_dictionary_api import EdisonBaseAPI


class sentimentsLanguagesController(EdisonBaseAPI):
    """Sentiment languages"""

    _controller_name = "sentimentsLanguagesController"

    def add(self, languageId, sentimentId):
        """

        Args:
            languageId: (string): languageId
            sentimentId: (string): sentimentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['languageId', 'sentimentId']
        parameters_names_map = {}
        api = '/sentiments-languages/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, sentimentId):
        """

        Args:
            sentimentId: (string): sentimentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentId']
        parameters_names_map = {}
        api = '/sentiments-languages/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, sentimentLanguageId):
        """

        Args:
            sentimentLanguageId: (string): sentimentLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentLanguageId']
        parameters_names_map = {}
        api = '/sentiments-languages/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, sentimentLanguageId):
        """

        Args:
            sentimentLanguageId: (string): sentimentLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['sentimentLanguageId']
        parameters_names_map = {}
        api = '/sentiments-languages/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
