from edison_dictionary_api import EdisonBaseAPI


class testGroupsController(EdisonBaseAPI):
    """Test groups"""

    _controller_name = "testGroupsController"

    def add(self, code, **kwargs):
        """

        Args:
            code: (string): code
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['code', '**kwargs']
        parameters_names_map = {}
        api = '/test-groups/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/test-groups/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, testGroupId):
        """

        Args:
            testGroupId: (string): testGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testGroupId']
        parameters_names_map = {}
        api = '/test-groups/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, testGroupId):
        """

        Args:
            testGroupId: (string): testGroupId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['testGroupId']
        parameters_names_map = {}
        api = '/test-groups/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
