from edison_dictionary_api import EdisonBaseAPI


class approversController(EdisonBaseAPI):
    """Approver - is someone who can approve edited by editors data in the database."""

    _controller_name = "approversController"

    def assignAll(self, editorsAssignment):
        """

        Args:
            editorsAssignment: (string): editorsAssignment

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['editorsAssignment']
        parameters_names_map = {}
        api = '/approvers/assignAll'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def assignCountry(self, approverId, dictionaryId, **kwargs):
        """

        Args:
            approverId: (string): approverId
            assign: (boolean): assign
            dictionaryId: (string): dictionaryId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['approverId', 'dictionaryId', '**kwargs']
        parameters_names_map = {}
        api = '/approvers/assignCountry'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def assignEditor(self, approverId, editorId, **kwargs):
        """

        Args:
            approverId: (string): approverId
            assign: (boolean): assign
            editorId: (string): editorId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['approverId', 'editorId', '**kwargs']
        parameters_names_map = {}
        api = '/approvers/assignEditor'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def assignTopic(self, approverId, dictionaryId, **kwargs):
        """

        Args:
            approverId: (string): approverId
            assign: (boolean): assign
            dictionaryId: (string): dictionaryId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['approverId', 'dictionaryId', '**kwargs']
        parameters_names_map = {}
        api = '/approvers/assignTopic'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def countriesList(self, **kwargs):
        """

        Args:
            approverId: (string): approverId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/approvers/countriesList'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def editorsList(self, **kwargs):
        """

        Args:
            approverId: (string): approverId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/approvers/editorsList'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self):
        """"""

        api_parameters = None
        parameters_names_map = {}
        api = '/approvers/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def listAll(self, approverId):
        """

        Args:
            approverId: (string): approverId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['approverId']
        parameters_names_map = {}
        api = '/approvers/listAll'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def topicsList(self, **kwargs):
        """

        Args:
            approverId: (string): approverId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/approvers/topicsList'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
