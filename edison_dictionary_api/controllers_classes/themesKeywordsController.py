from edison_dictionary_api import EdisonBaseAPI


class themesKeywordsController(EdisonBaseAPI):
    """Themes keywords"""

    _controller_name = "themesKeywordsController"

    def add(self, keyword, themeLanguageId, weight):
        """

        Args:
            keyword: (string): keyword
            themeLanguageId: (string): themeLanguageId
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keyword', 'themeLanguageId', 'weight']
        parameters_names_map = {}
        api = '/themes-keywords/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, keywordId, weight):
        """

        Args:
            keywordId: (string): keywordId
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId', 'weight']
        parameters_names_map = {}
        api = '/themes-keywords/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, themeLanguageId, **kwargs):
        """

        Args:
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sortMode: (string): { "name", "weight" }
            sortOrderAscending: (boolean): true = ascending (default); false = descending
            themeLanguageId: (string): themeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['themeLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/themes-keywords/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def listForAllThemes(self, **kwargs):
        """

        Args:
            languageId: (string): languageId
            minLevel: (number): minLevel

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/themes-keywords/listForAllThemes'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/themes-keywords/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, keywordId):
        """

        Args:
            keywordId: (string): keywordId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['keywordId']
        parameters_names_map = {}
        api = '/themes-keywords/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
