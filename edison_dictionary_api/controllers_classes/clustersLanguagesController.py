from edison_dictionary_api import EdisonBaseAPI


class clustersLanguagesController(EdisonBaseAPI):
    """Clusters languages"""

    _controller_name = "clustersLanguagesController"

    def add(self, clusterId, languageId):
        """

        Args:
            clusterId: (string): clusterId
            languageId: (string): languageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterId', 'languageId']
        parameters_names_map = {}
        api = '/clusters-languages/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, clusterId):
        """

        Args:
            clusterId: (string): clusterId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterId']
        parameters_names_map = {}
        api = '/clusters-languages/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, clusterLanguageId):
        """

        Args:
            clusterLanguageId: (string): clusterLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterLanguageId']
        parameters_names_map = {}
        api = '/clusters-languages/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, clusterLanguageId):
        """

        Args:
            clusterLanguageId: (string): clusterLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterLanguageId']
        parameters_names_map = {}
        api = '/clusters-languages/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
