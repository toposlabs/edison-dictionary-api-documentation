from edison_dictionary_api import EdisonBaseAPI


class topicTypesDocumentsController(EdisonBaseAPI):
    """Topic-types documents"""

    _controller_name = "topicTypesDocumentsController"

    def add(self, text, topicTypeLanguageId, **kwargs):
        """

        Args:
            text: (string): text
            topicTypeLanguageId: (string): topicTypeLanguageId
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['text', 'topicTypeLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/topic-types-documents/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def addFile(self, files, topicTypeLanguageId, **kwargs):
        """Supported file formats: https://tika.apache.org/1.13/formats.html

        Args:
            files: (array): files
            splitByLines: (boolean): splitByLines
            topicTypeLanguageId: (string): topicTypeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['files', 'topicTypeLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/topic-types-documents/addFile'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, topicTypeDocumentId, **kwargs):
        """

        Args:
            text: (string): text
            topicTypeDocumentId: (string): topicTypeDocumentId
            weight: (number): weight

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['topicTypeDocumentId', '**kwargs']
        parameters_names_map = {}
        api = '/topic-types-documents/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, topicTypeLanguageId, **kwargs):
        """

        Args:
            maxLength: (integer): maxLength
            maxNumber: (integer): maxNumber
            offset: (integer): offset
            sortMode: (string): { "weight", "proximity", "doc2vec", "keywords", "entities" }
            sortOrderAscending: (boolean): true = ascending (default); false = descending
            topicTypeLanguageId: (string): topicTypeLanguageId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['topicTypeLanguageId', '**kwargs']
        parameters_names_map = {}
        api = '/topic-types-documents/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, topicTypeDocumentId):
        """

        Args:
            topicTypeDocumentId: (string): topicTypeDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['topicTypeDocumentId']
        parameters_names_map = {}
        api = '/topic-types-documents/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, topicTypeDocumentId):
        """

        Args:
            topicTypeDocumentId: (string): topicTypeDocumentId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['topicTypeDocumentId']
        parameters_names_map = {}
        api = '/topic-types-documents/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
