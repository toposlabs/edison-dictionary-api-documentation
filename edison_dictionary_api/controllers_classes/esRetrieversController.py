from edison_dictionary_api import EdisonBaseAPI


class esRetrieversController(EdisonBaseAPI):
    """Retrieve documents from ElasticSearch."""

    _controller_name = "esRetrieversController"

    def add(self, host, index, name, port, **kwargs):
        """

        Args:
            description: (string): description
            host: (string): host
            index: (string): index
            name: (string): name
            port: (integer): port

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['host', 'index', 'name', 'port', '**kwargs']
        parameters_names_map = {}
        api = '/es-retrievers/add'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def assign(self, editorId, retrieverId, **kwargs):
        """

        Args:
            assign: (boolean): assign
            editorId: (string): editorId
            retrieverId: (string): retrieverId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['editorId', 'retrieverId', '**kwargs']
        parameters_names_map = {}
        api = '/es-retrievers/assign'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def edit(self, id, **kwargs):
        """

        Args:
            description: (string): description
            host: (string): host
            id: (string): id
            index: (string): index
            port: (integer): port

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id', '**kwargs']
        parameters_names_map = {}
        api = '/es-retrievers/edit'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def getDocument(self, retrieverId):
        """

        Args:
            retrieverId: (string): retrieverId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['retrieverId']
        parameters_names_map = {}
        api = '/es-retrievers/getDocument'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def getDocumentsToCluster(self, clusterLanguageId, documentsNumber, retrieverId):
        """

        Args:
            clusterLanguageId: (string): clusterLanguageId
            documentsNumber: (integer): documentsNumber
            retrieverId: (string): retrieverId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['clusterLanguageId', 'documentsNumber', 'retrieverId']
        parameters_names_map = {}
        api = '/es-retrievers/getDocumentsToCluster'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def list(self, **kwargs):
        """

        Args:
            editorId: (string): editorId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['**kwargs']
        parameters_names_map = {}
        api = '/es-retrievers/list'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def remove(self, id):
        """

        Args:
            id: (string): id

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['id']
        parameters_names_map = {}
        api = '/es-retrievers/remove'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)

    def retrieve(self, retrieverId):
        """

        Args:
            retrieverId: (string): retrieverId

        Returns:
            application/json;charset=UTF-8
        """

        api_parameters = ['retrieverId']
        parameters_names_map = {}
        api = '/es-retrievers/retrieve'
        actions = ['post']
        params = self._format_params_for_api(locals(), api_parameters, parameters_names_map)
        return self._process_api(self._controller_name, api, actions, params)
