name = "edison_dictionary_api"
import errno
import hashlib
# -*- coding: utf-8 -*-
import json
import os
import re
import requests
import sys
import tempfile
import time
from inspect import signature
from requests.packages.urllib3.exceptions import InsecureRequestWarning

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), 'controllers_classes'))
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__))))

from EdisonAPIClassLoader import EdisonApiClassLoader
from EdisonErrors import EdisonAPIResponseError
from EdisonErrors import EdisonAuthError
from EdisonErrors import EdisonError
from EdisonErrors import MissingDirectory
from EdisonErrors import FileAccessError

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

THIS_FILE_PATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__)))
if 'CONTROLLERS_CLASSES_PATH' in os.environ:
    CONTROLLERS_CLASSES_PATH = os.environ['CONTROLLERS_CLASSES_PATH']
else:
    CONTROLLERS_CLASSES_PATH = 'controllers_classes'


class EdisonBaseAPI(EdisonAPIResponseError, EdisonError, EdisonAuthError):
    _session = None

    def __init__(self):
        pass

    def _format_params_for_api(self, params=None, parameters_names=None, parameters_names_map={}):
        skip_vars = ['api', 'actions']

        if not params:
            return

        ret_key_vales = {}
        for var_name in params.keys():
            if var_name != 'self' and params[var_name] is not None and var_name not in skip_vars:

                if var_name == 'kwargs':
                    for v_n in params[var_name]:
                        if v_n in parameters_names_map:
                            v_n = parameters_names_map[v_n]
                        ret_key_vales.update({v_n: params[var_name][v_n]})
                else:
                    if parameters_names and var_name not in parameters_names:
                        continue

                    # some params are not compatible with python
                    # map back to the correct param name for the Edison API
                    if var_name in parameters_names_map:
                        var_name = parameters_names_map[var_name]

                    ret_key_vales.update({var_name: params[var_name]})
        return ret_key_vales

    def _start_session(self):
        if self._session:
            self._close_session()

        self._session = requests.session()

        login_data = self._controller_classes['loginController'].login(self, login=self._login_name,
                                                                       password=self._login_password)
        if 'status' in login_data and login_data['status'] is False:
            raise BaseException(login_data['message'])

    def _close_session(self):
        self._session = None

    def _prepare_file_for_upload(self, file):
        if not os.path.isfile(file):
            raise EdisonAPI('Missing upload file', file)

        return {'file': open(file, 'rb')}

    def _prepare_files_for_upload(self, files):
        if type(files) is not list:
            files = [files]

        upload_files = []
        for upload_file in files:
            if not os.path.isfile(upload_file):
                raise EdisonAPI('Missing upload file', upload_file)

            file_tuple = ('files', open(upload_file, 'rb'))
            upload_files.append(file_tuple)

        if len(upload_files) < 1:
            return None
        return upload_files

    def _md5(self, file_name):
        try:
            hash_md5 = hashlib.md5()
            with open(file_name, "rb") as f:
                for chunk in iter(lambda: f.read(4096), b""):
                    hash_md5.update(chunk)
        except:
            raise FileAccessError('Error creating file hash for download file %s' % file_name)

        return hash_md5.hexdigest()

    def _process_api(self, controller_name, api, actions, params):
        if not self._session:
            self._start_session()

        files = None
        if 'file' in params:
            files = self._prepare_file_for_upload(params['file'])
            del params['file']
        elif 'files' in params:
            files = self._prepare_files_for_upload(params['files'])
            del params['files']

        args = self._format_params_for_api(params)
        api_url = '%s://%s:%s%s' % (self._http_protocol, self._host, self._port, api)

        if 'post' in actions:
            session_action = self._session.post
        elif 'get' in actions:
            session_action = self._session.get
        elif 'put' in actions:
            session_action = self._session.put
        elif 'delete' in actions:
            session_action = self._session.delete
        else:
            raise EdisonError('Bad http action %s attempted on api: %s' % (','.join(actions), api))

        if not files:
            response = session_action(api_url, data=args, verify=self._verify_ssl, proxies=self._proxies)
        else:
            response = session_action(api_url, data=args, files=files, verify=self._verify_ssl, proxies=self._proxies)

        if response.status_code == 401:
            if controller_name == 'logoutController':
                raise EdisonAPIResponseError(401, 'Authentication required')
            else:
                raise EdisonAPIResponseError(401, 'Invalid session')

        if response.status_code == 403:
            raise EdisonAPIResponseError(403, 'Access is forbidden')

        elif 'content-type' in response.headers and response.headers[
            'content-type'].lower() == 'application/octet-stream':
            file_name = re.findall('filename=(.+)', response.headers.get('content-disposition'))[0]
            save_file = os.path.join(self._download_location, file_name)
            if os.path.exists(save_file):
                try:
                    os.unlink(save_file)
                except OSError as e:
                    if e.errno != errno.ENOENT:  # errno.ENOENT = no such file or directory
                        raise

            try:
                with open(save_file, 'wb') as fd:
                    for chunk in response.iter_content(chunk_size=128):
                        fd.write(chunk)
            except OSError as e:
                raise FileAccessError('Unable to save file %s - %s' % (file_name, e))
            return {
                'downloadFile': save_file,
                'fileSize': os.path.getsize(save_file),
                'fileHash': self._md5(save_file)
            }

        elif not response.status_code == 200 or response.json()['status'] is False:
            try:
                response_data = json.loads(response.content)
            except:
                raise EdisonError('Unable to "%s" on api "%s": (%s)' % (','.join(actions), api, response.content))
            raise EdisonError('Unable to "%s" on api "%s": %s' % (
                ','.join(actions), api, response_data['message']))

        if hasattr(response, 'json'):
            return response.json()['response']


class EdisonAPI(EdisonApiClassLoader, EdisonBaseAPI):
    """EdisonAPI - Edison AI platform API wrapper.

    Attributes:
        wait_for_task_complete: Wait for long running tasks to complete before response
        remove_task_data_on_complete: Remove task data from long running task once complete without error
    """

    _login_name = None
    _login_password = None
    _host = None
    _port = 443
    _verify_ssl = False
    _http_protocol = 'https'
    _proxies = None
    _download_location = None

    _class_loader = None
    _api_controllers = []
    _controller_name = None

    wait_for_task_complete = False
    remove_task_data_on_complete = True

    def __init__(self, host, port, login_name, login_password, require_ssl=True, verify_ssl=False,
                 wait_for_task_complete=False,
                 remove_task_data_on_complete=True,
                 proxies=None,
                 download_file_location=None):

        self._host = host
        self._port = port
        self._login_name = login_name
        self._login_password = login_password
        self.wait_for_task_complete = wait_for_task_complete
        self.remove_task_data_on_complete = remove_task_data_on_complete
        self._proxies = proxies

        if download_file_location:
            self._download_location = download_file_location
        else:
            self._download_location = tempfile.gettempdir()
        if not os.path.isdir(self._download_location):
            raise MissingDirectory('Missing download directory %s' % self._download_location)

        if require_ssl:
            self._http_protocol = 'https'
        else:
            self._http_protocol = 'http'

        self._verify_ssl = verify_ssl

        EdisonBaseAPI.__init__(self)
        EdisonApiClassLoader.__init__(self, CONTROLLERS_CLASSES_PATH)

    def _get_api_from_controller(self, controller_name, api):
        if controller_name not in self._controller_classes:
            raise EdisonError('Invalid controller')

        if api not in self._controller_classes[controller_name].__dict__.keys():
            raise EdisonError('API does not exist in controller')

        api_class_method = getattr(self._controller_classes[controller_name], api)
        sig = signature(api_class_method)

        function_params = ['self']
        has_kawargs = False
        for k, v in sig.parameters.items():
            if k == 'kwargs':
                has_kawargs = True
            elif k != 'self':
                function_params.append(str(v))
        if has_kawargs:
            function_params.append('**kwargs')

        return len(sig.parameters), api_class_method, function_params

    def _wait_for_task_results(self, task_data, remove_results=True, sleep_time=1):
        while True:
            if task_data and 'taskId' in task_data:
                task_data = self.call_api('tasksController', 'retrieve', id=task_data['taskId'])
                if task_data and 'status' in task_data and \
                        type(task_data['status']) is str and \
                        (task_data['status'].lower() == 'completed' or task_data['status'].lower() == 'failed'):
                    break
            time.sleep(sleep_time)

        if remove_results:
            self.call_api('tasksController', 'remove', id=task_data['taskId'])

        if task_data['errorMessage']:
            raise EdisonError(task_data['errorMessage'])

        return task_data['result']

    def call_api(self, controller_name, api, **kwargs):
        """Edison API request

                Args:
                    controller_name: (string): Name of Edison controller
                    api: (string): Name of Edison controller api
                    **kargs: Name/value pairs of API params
                Returns:
                    API response
        """

        arg_len, api_obj, function_params = self._get_api_from_controller(controller_name, api)
        self._controller_name = controller_name
        function_sig = 'api_obj(%s)' % ', '.join(function_params)

        _kwargs = None
        if kwargs:
            thismodule = sys.modules[__name__]
            _kwargs = kwargs
            for p in function_params:
                found = False
                if p in _kwargs:
                    setattr(thismodule, p, kwargs[p])
                    del _kwargs[p]
                    found = True
                if not found and p != 'self' and p != '**kwargs':
                    function_params.remove('self')
                    raise EdisonError('Missing API parameter(s) %s' % ', '.join(function_params),
                                      'Required parameter: %s' % ', '.join(function_params))

        if _kwargs and len(_kwargs) > 0:
            kwargs = _kwargs

        counter = 0
        while True:
            counter += 1
            try:
                api_ret_data = eval(function_sig)
                if controller_name != 'tasksController' and 'taskId' in api_ret_data and self.wait_for_task_complete:
                    return self._wait_for_task_results(api_ret_data, self.remove_task_data_on_complete)
                return api_ret_data

            except EdisonAPIResponseError as e:
                if counter > 2:
                    raise EdisonError('API error: (%s) %s' % (e.parameter, e.message))

                if e.message == 'Invalid session':
                    self._session = None
                else:
                    break
