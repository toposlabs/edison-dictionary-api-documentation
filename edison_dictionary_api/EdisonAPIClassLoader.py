import os
import pkgutil
import sys

from EdisonErrors import edisonAPIClassLoaderErrorException


class EdisonApiClassLoader(edisonAPIClassLoaderErrorException):
    _controller_classes = {}

    def __init__(self, class_directory):
        if not os.path.exists(os.path.join(os.path.dirname(os.path.abspath(__file__)), class_directory)):
            raise edisonAPIClassLoaderErrorException('Class directory does not exist', class_directory)

        self._load_classes(class_directory)

    def get_controller_classes(self):
        return self._controller_classes

    def _load_classes(self, class_directory):
        modules = pkgutil.iter_modules(path=[os.path.join(os.path.dirname(os.path.abspath(__file__)), class_directory)])

        for loader, mod_name, ispkg in modules:
            if mod_name not in sys.modules:
                loaded_mod = __import__(class_directory + "." + mod_name, fromlist=[mod_name], globals=globals())
                loaded_class = getattr(loaded_mod, mod_name)
                loaded_class.__init__(self)
                self._controller_classes.update({mod_name: loaded_class})
