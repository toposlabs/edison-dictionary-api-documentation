class FileAccessError(Exception):
    def __init__(self, value, message=""):
        self.parameter = value
        self.message = message

    def __str__(self):
        return repr(self.parameter)

class MissingDirectory(Exception):
    def __init__(self, value, message=""):
        self.parameter = value
        self.message = message

    def __str__(self):
        return repr(self.parameter)

class EdisonError(Exception):
    def __init__(self, value, message=""):
        self.parameter = value
        self.message = message

    def __str__(self):
        return repr(self.parameter)


class EdisonAPIResponseError(Exception):
    def __init__(self, value, message):
        self.parameter = value
        self.message = message

    def __str__(self):
        return repr(self.parameter)


class EdisonAuthError(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.parameter)


class edisonAPIClassLoaderErrorException(Exception):
    def __init__(self, message, *args):
        self.message = message
        self.args = args

    def __str__(self):
        return repr(self.parameter)
