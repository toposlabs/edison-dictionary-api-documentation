https://readthedocs.org/projects/edison-dictionary-api-documentation/

1) copy over edison_dictionary_api, package_templates, and source_dictionary from swagger-api-parser project
2) sphinx-build -b html source_dictionary _build
3) test from _build directory
4) remove _build
5) commit changes
6) goto https://readthedocs.org/projects/edison-dictionary-api-documentation/
7) Click Build version button